﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RazorAsyncTask
{
    public class UserDatabase
    {
        public IEnumerable<User> Users()
        {
            return new List<User>()
            {
                new User() {
                    Username = "amy", AccountNumber = 1111111111, 
                    Email = "sapase0@gmail.com", Address = "0 sapa street, centenary, Enugu"
                },
                new User() {
                    Username = "sam", AccountNumber = 2222222222, 
                    Email = "sapase1@gmail.com", Address = "1 sapa street, centenary, Enugu"
                },
                new User() {
                    Username = "paul", AccountNumber = 3333333333, 
                    Email = "sapase2@gmail.com", Address = "2 sapa street, centenary, Enugu"
                },
                new User() {
                    Username = "jude", AccountNumber = 4444444444, 
                    Email = "sapase3@gmail.com", Address = "3 sapa street, centenary, Enugu"
                },
            };
        }
    }
}
