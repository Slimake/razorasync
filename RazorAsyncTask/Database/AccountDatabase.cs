﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RazorAsyncTask
{
    public class AccountDatabase
    {
        public IEnumerable<Login> Logins()
        {
            return new List<Login>()
            {
                new Login() {username = "amy", password = "amy123"},
                new Login() {username = "sam", password = "sam123"},
                new Login() {username = "paul", password = "paul123"},
                new Login() {username = "jude", password = "jude123"},
            };
        }
    }
}
