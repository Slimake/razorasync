﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RazorAsyncTask
{
    public static class Application
    {
        public static void Run()
        {
            StringBuilder StringBuilder = new StringBuilder();
            
            mainmenu: 
                StringBuilder.Clear();
                StringBuilder.AppendLine("Welcome to Razor Finance App");
                StringBuilder.AppendLine("Press: ");
                StringBuilder.AppendLine("1. Login");
                StringBuilder.AppendLine("2. Exit");

            var running = true;

            while(running)
            {
                Console.WriteLine(StringBuilder.ToString());
                switch(Console.ReadLine())
                {
                    case "1":
                        LoginUI.Run();
                        DisplayPrompt();
                        if (Console.ReadLine() == "1")
                            goto mainmenu;
                        else
                            running = false;
                        break;
                    case "2":
                        running = false;
                        Console.WriteLine("\nGoodBye!...\nRun the App to use again!...\n");
                        break;

                    default:
                        Console.WriteLine("\nInvalid input...\nTry Again!!\n");
                        goto mainmenu;
                }

            }
        }

        public static void DisplayPrompt()
        {
            Console.ReadLine();
            Console.WriteLine("\nDo you want to perform another operation? \n1. Yes\n2. Any other Key to exit!");
        }
    }
}
