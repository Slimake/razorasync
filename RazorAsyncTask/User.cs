﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RazorAsyncTask
{
    public class User
    {
        public string Username { get; set; }
        public long AccountNumber { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
    }
}
