﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RazorAsyncTask
{
    public static class LoginUI
    {
        public static AccountDatabase Database = new AccountDatabase();
        public static UserDatabase UserDB = new UserDatabase();
        public async static void Run()
        {

            Console.WriteLine("Enter your username");
            var username = Console.ReadLine();

            while (String.IsNullOrWhiteSpace(username))
            {
                Console.WriteLine("Blank field not allowed\nEnter a valid username");
                username = Console.ReadLine();
            }

            Console.WriteLine("Enter your password");
            var password = Console.ReadLine();

            while (String.IsNullOrWhiteSpace(password))
            {
                Console.WriteLine("Blank field not allowed\nEnter a valid password");
                password = Console.ReadLine();
            }

            var accountDBTask = QueryAccountDB(username, password);
            var userDBTask = QueryUserDatabase(username);

            var valid = await accountDBTask;
            var profile = await userDBTask;

            if(valid == true)
            {
                Console.WriteLine(profile);
                
            }
            else
            {
                Console.WriteLine("Username or Password is incorrect");
            }
        }

        public static async Task<bool> QueryAccountDB(string fieldUsername, string fieldPassword)
        {
            Console.WriteLine("Querying account db");
            var userQuery = from user in Database.Logins()
                        where user.username == fieldUsername && user.password == fieldPassword
                        select user.username;
            await Task.Delay(5000);
            if (userQuery.FirstOrDefault() != null)
            {
                Console.WriteLine($"\nHello {userQuery.First()}\nYour details are: \n");
                return true;
            }
            else
            {
                return false;
            }
        }

        public static async Task<string> QueryUserDatabase(string username)
        {
            Console.WriteLine("querying user db");
            await Task.Delay(3000);
            var userQuery = UserDB.Users().Where(user => user.Username == username);

            var user = userQuery.FirstOrDefault();

            StringBuilder StringBuilder = new StringBuilder();
            StringBuilder.AppendLine($"Account number: {user.AccountNumber}");
            StringBuilder.AppendLine($"Email: {user.Email}");
            StringBuilder.AppendLine($"Address: {user.Address}");

            return StringBuilder.ToString();
        }
    }
}
